# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import tensorflow as tf
import math
import heapq

import keras
from keras import backend as K
from keras import initializers
from keras.regularizers import l1, l2, l1_l2
from keras.models import Sequential, Model
from keras.layers.core import Dense, Lambda, Activation
from keras.layers import Embedding, Input, Dense, Reshape, Flatten, Dropout
from keras.optimizers import Adagrad, Adam, SGD, RMSprop
from keras import backend as K
from time import time
import sys

import pickle

##parameters
top_k = 10
epochs = 2
learn_rate = 0.0001
batch_size = 256

last_layer_size = 128
layers = [128,64,32,16]
layers_DMF = 256
reg_layers = [0,0,0,0]

best_time = -1
best_iter = -1
num_negs =5#neg_ratio
neg_sample_size = 1 * num_negs
train_batch_size= batch_size + neg_sample_size*batch_size
n_pair = num_negs+1
test_batch_size=100
model_path = "./trained-JNCF-TP-model.ckpt"
trained = False
final_ndcg_metric_list = []
final_hr_metric_list = []

alpha = 0.5

def get_user_item_interaction_map(file):
    ##read file
    data_dense = pd.read_table(file,header=None, names=['user_id','item_id','rate','time'])
    u_max_num=data_dense.user_id.nunique()
    v_max_num=data_dense.item_id.nunique()
    
    #recode the user_id from 0 to ...
    suoyin1=pd.DataFrame({'user_id':data_dense.user_id.unique(),'data2':range(u_max_num)})
    data_haha=pd.merge(suoyin1,data_dense,on='user_id')
    data_haha=data_haha.drop('user_id',axis=1)
    data_haha.rename(columns={'data2':'user_id'}, inplace=True)
    
    #recode the item_id from 0 to ...    
    data_haha=data_haha.sort_values(by=['item_id','user_id'])
    suoyin2=pd.DataFrame({'item_id':data_haha.item_id.unique(),'data2':range(v_max_num)})
    data_pruned=pd.merge(suoyin2,data_haha,on='item_id')
    data_pruned=data_pruned.drop('item_id',axis=1)
    data_pruned.rename(columns={'data2':'item_id'}, inplace=True)
    
    #change to 'user_id, item_id, rate, time'
    u_id=data_pruned.pop('user_id')
    data_pruned.insert(0,'user_id',u_id)
    data_pruned_0=data_pruned
    
    ##the lastest rate for every user
    data_last=data_pruned.sort_values(by=['time','rate']).groupby('user_id')
    user_latest_item=data_last.last() 
    user_latest_item4 = user_latest_item.reset_index(drop=False)
    data_pruned=data_pruned.append(user_latest_item4)
    data_pruned=data_pruned.append(user_latest_item4)
    data_pruned = data_pruned.drop_duplicates(subset=['user_id','item_id','rate','time'],keep=False)
    
    #iteration version of data_pruned
    data_iteration_0=list(zip(data_pruned_0['user_id'],data_pruned_0['item_id'],data_pruned_0['rate'],data_pruned_0['time']))
    data_iteration=list(zip(data_pruned['user_id'],data_pruned['item_id'],data_pruned['rate'],data_pruned['time']))

    #dictionary for user-item rate
    user_map_item={}
    for u,i,r,t in data_iteration_0:
        if u not in user_map_item:
            user_map_item[u]={}
        user_map_item[u][i]=r
        
    latest_item_interaction={}
    for u,i,r,t in data_iteration_0:
        if u not in latest_item_interaction:
            latest_item_interaction[u]=[]
            latest_item_interaction[u]=user_latest_item.values[u][0]   
    return data_iteration_0, data_iteration, user_map_item, latest_item_interaction, u_max_num, v_max_num

pruned_all_ratings_0, pruned_all_ratings, user_map_item, latest_item_interaction,u_max_num,v_max_num = get_user_item_interaction_map('/home/data.data')

pruned_user_map_item = {}
pruned_item_map_user = {}
for u, v, r, t in pruned_all_ratings:
    if u not in pruned_user_map_item:
        pruned_user_map_item[u] = {}
    if v not in pruned_item_map_user:
        pruned_item_map_user[v] = {}
    pruned_user_map_item[u][v] = r
    pruned_item_map_user[v][u] = r

max_u = {}
for u_i in pruned_user_map_item:
    max_u[u_i] = max(pruned_user_map_item[u_i])

class MySampler():
    def __init__(self, all_ratings, u_max_num, v_max_num):
        self.sample_con = {}
        self.sample_con_size = 0
        self.all_ratings_map_u = {}
        for u, v, r, t in all_ratings:
            if u not in self.all_ratings_map_u:
                self.all_ratings_map_u[u] = {}
            self.all_ratings_map_u[u][v] = 1

        self.u_max_num = u_max_num
        self.v_max_num = v_max_num

    def smple_one(self, u_i):
         #u_rand_num = int(np.random.rand() * self.u_max_num)
         v_rand_num = int(np.random.rand() * (self.v_max_num))
         if v_rand_num not in self.all_ratings_map_u[u_i] and v_rand_num in pruned_item_map_user:
            return v_rand_num
         else:
            return self.smple_one(u_i)

my_sample = MySampler(pruned_all_ratings_0, u_max_num, v_max_num)


def getHitRatio(ranklist, gtItem):
    for item in ranklist:
        if item == gtItem:
           return 1.0
    return 0.0


def getNDCG(ranklist, gtItem):
    for i in range(len(ranklist)):
         item = ranklist[i]
         if item == gtItem:
            return math.log(2) / math.log(i + 2)
    return 0.0


#########################------------data_prepare_4_train_random-----------------#####################################################
if not trained:
    set_4_batch_train = []
    np.random.shuffle(pruned_all_ratings)
    for u_i, v_i, r_i, t_i in pruned_all_ratings:
        batch_v= []
        batch_v.append(v_i)
        sam=0
        v_sam=[]
        while sam < (neg_sample_size):
            v_neg = my_sample.smple_one(u_i)
            if v_neg not in v_sam:
                batch_v.append(v_neg)
                sam=sam+1
                v_sam.append(v_neg)
        set_4_batch_train.append([u_i,batch_v,r_i,t_i])
    with open("set_4_batch_train_TP.txt", "wb") as fp:
        pickle.dump(set_4_batch_train, fp)
elif trained:
    with open("set_4_batch_train_TP.txt", "rb") as fp:
        set_4_batch_train = pickle.load(fp)

#########################------------data_prepare_4_test-----------------#####################################################
if not trained:
    set_4_batch_test = {}
    for u_i in latest_item_interaction:
        if u_i not in set_4_batch_test:
            set_4_batch_test[u_i] = []
            v_latest = latest_item_interaction[u_i]
            v_random = [v_latest]
            i = 1
            while i < 100:
                rand_num = int(np.random.rand() * (v_max_num - 1) + 1)
                if rand_num not in user_map_item[u_i] and rand_num not in v_random and rand_num in pruned_item_map_user:
                    v_random.append(rand_num)
                    i += 1
            set_4_batch_test[u_i].append(v_random)
    with open("set_4_batch_test_TP.txt", "wb") as fp:
        pickle.dump(set_4_batch_test, fp)

elif trained:
    with open("set_4_batch_test_TP.txt", "rb") as fp:
        set_4_batch_test = pickle.load(fp)



#########################------------design model-----------------#####################################################

if __name__ == "__main__":    
    print("J-NCF begins")
    with tf.variable_scope('optimizer', reuse=tf.AUTO_REUSE):
        num_layer = len(layers) #Number of layers in the MLP
        init = tf.random_normal_initializer(mean=0,stddev=0.01)

        one_hot_u = tf.placeholder(tf.float32, [None, v_max_num])
        one_hot_vp = tf.placeholder(tf.float32, [None, u_max_num])
        one_hot_vn = tf.placeholder(tf.float32, [None, num_negs,u_max_num])
        one_true_u_v = tf.placeholder(tf.float32, [None, num_negs+1,1])

        one_hot_vnn = tf.transpose(one_hot_vn, perm=[1, 0, 2])
        one_true_u_vv = tf.transpose(one_true_u_v, perm=[1, 0, 2])

        gmf_loss_pp =0.0
        gmf_loss_point = 0.0

        u_w1 = tf.get_variable("u_w1", shape=(v_max_num, layers_DMF),initializer=init)
        u_b1 = tf.get_variable("u_b1", shape=[layers_DMF],initializer=init)
        u_w2 = tf.get_variable("u_w2", shape=(layers_DMF, last_layer_size),initializer=init)
        u_b2 = tf.get_variable("u_b2", shape=[last_layer_size],initializer=init)
        
        v_w1 = tf.get_variable("v_w1", shape=(u_max_num, layers_DMF),initializer=init)
        v_b1 = tf.get_variable("v_b1", shape=[layers_DMF],initializer=init)
        v_w2 = tf.get_variable("v_w2", shape=(layers_DMF, last_layer_size),initializer=init)
        v_b2 = tf.get_variable("v_b2", shape=[last_layer_size],initializer=init)
        
        net_u_1 = tf.nn.relu(tf.matmul(one_hot_u, u_w1) + u_b1)
        net_u_2 = tf.matmul(net_u_1, u_w2) + u_b2
        
        net_v_1 = tf.nn.relu(tf.matmul(one_hot_vp, v_w1) + v_b1)
        net_v_2 = tf.matmul(net_v_1, v_w2) + v_b2

        predict_vector2 = keras.layers.Multiply()([net_u_2, net_v_2])
        for idx in range(num_layer):
            layer = Dense(layers[idx], W_regularizer= l2(reg_layers[idx]),activation='relu', name="layer%d" %idx)
            predict_vector2 = layer(predict_vector2)
        pred_val = Dense(1, activation='sigmoid', init='lecun_uniform', name = "prediction")(predict_vector2)
        one_constant = tf.constant(1.0, shape=[1, 1])
        gmf_loss_positive = tf.reduce_mean(-one_true_u_vv[0] * tf.log(pred_val + 1e-10) - (one_constant - one_true_u_vv[0]) * tf.log(one_constant - pred_val + 1e-10))



        for i in range(num_negs):    
            net_v_n11 = tf.nn.relu(tf.matmul(one_hot_vnn[i], v_w1) + v_b1)
            net_v_n12 = tf.matmul(net_v_n11, v_w2) + v_b2
            predict_vector2n = keras.layers.Multiply()([net_u_2, net_v_n12])

            for idx in range(num_layer):
                layer = Dense(layers[idx], W_regularizer= l2(reg_layers[idx]),activation='relu', name="layer%d" %idx)
                predict_vector2n = layer(predict_vector2n)
            prediction_nn = Dense(1, activation='sigmoid', init='lecun_uniform', name = "prediction")(predict_vector2n)
            
            gmf_loss_negative = tf.reduce_mean(-one_true_u_vv[i+1] * tf.log(prediction_nn + 1e-10) - (one_constant - one_true_u_vv[i+1]) * tf.log(one_constant - prediction_nn + 1e-10))

            gmf_loss_point = gmf_loss_negative +gmf_loss_point

            gmf_loss_pp = gmf_loss_pp + tf.sigmoid(prediction_nn-pred_val)+tf.sigmoid(tf.multiply(prediction_nn,prediction_nn))

        gmf_loss_pair = tf.reduce_mean(gmf_loss_pp/num_negs)
        gmf_loss_pointt = gmf_loss_point + gmf_loss_positive

        gmf_loss = (1.0-alpha)*gmf_loss_pair + alpha*gmf_loss_pointt

        
        train_step=tf.train.AdamOptimizer(learn_rate).minimize(gmf_loss)
        
        saver = tf.train.Saver()

        sess = tf.InteractiveSession()
        tf.initialize_all_variables().run()

        if trained:
            saver.restore(sess, model_path)
            epochs = -1
            epoch = -1
            print("load done")

        ##########################################################-------first evaluation------##########################################################
        batch_u_test = np.zeros((100, v_max_num)).astype('float32')
        batch_v_test = np.zeros((100, u_max_num)).astype('float32')
        hr_list = []
        ndcg_list = []
        prediction_time = 0
        for u_i in latest_item_interaction:
            t_init = time()
            v_random = set_4_batch_test[u_i][0]
            for i in range(100):
                for v_in in pruned_user_map_item[u_i]:
                    batch_u_test[i][v_in] = pruned_user_map_item[u_i][v_in]
                for u_in in pruned_item_map_user.get(v_random[i], []):
                    batch_v_test[i][u_in] = pruned_item_map_user[v_random[i]][u_in]
            pred_value = sess.run([pred_val], feed_dict={one_hot_u: batch_u_test, one_hot_vp: batch_v_test})
            pre_real_val = np.array(pred_value).reshape((-1))
            for i in range(100):
                for v_in in pruned_user_map_item[u_i]:
                    batch_u_test[i][v_in] = 0.0
                for u_in in pruned_item_map_user.get(v_random[i], []):
                    batch_v_test[i][u_in] = 0.0
            items = v_random
            gtItem = items[0]
            map_item_score = {}
            for i in range(len(items)):
                item = items[i]
                map_item_score[item] = pre_real_val[i]
            ranklist = heapq.nlargest(top_k, map_item_score, key=map_item_score.get)
            prediction_time = prediction_time + time() - t_init
            hr_list.append(getHitRatio(ranklist, gtItem))
            ndcg_list.append(getNDCG(ranklist, gtItem))
        hr_val, ndcg_val = np.array(hr_list).mean(), np.array(ndcg_list).mean()
        print('Initial(end): HR = %.4f, NDCG = %.4f. prediction time = %.4f s]' % (hr_val, ndcg_val, prediction_time))
        final_hr_metric_list.append(hr_val)
        final_ndcg_metric_list.append(ndcg_val)
        best_time = prediction_time

        ##################################################################--------begin training------##########################################################
        t_begin = time()
        batch_u_test = np.zeros((100, v_max_num)).astype('float32')
        batch_v_test = np.zeros((100, u_max_num)).astype('float32')
        for epoch in range(epochs):
            t_epoch_begin = time()
            one_epoch_loss = 0.0
            one_epoch_batchnum = 0.0
            np.random.shuffle(set_4_batch_train)
            for index in range(int(len(set_4_batch_train) / batch_size)):
                batch_u = np.zeros((batch_size, v_max_num)).astype('float32')
                batch_vp = np.zeros((batch_size, u_max_num)).astype('float32')
                batch_vn = np.zeros((batch_size, num_negs,u_max_num)).astype('float32')
                batch_true_u_v = np.zeros((batch_size, num_negs+1, 1)).astype('float32')
                for i in range(0, batch_size):
                    start = index*batch_size +i
                    u_i = set_4_batch_train[start][0]
                    v_set = set_4_batch_train[start][1]
                    r_i  = set_4_batch_train[start][2]
                    t_i = set_4_batch_train[start][3]
                    v_i = v_set[0]
                    for v_in in pruned_user_map_item[u_i]:
                        batch_u[i][v_in] = pruned_user_map_item[u_i][v_in]
                    for u_in in pruned_item_map_user.get(v_i, []):
                        batch_vp[i][u_in] = pruned_item_map_user[v_i][u_in]
                    batch_true_u_v[i][0][0] = 1.0

                    for j in range(1, len(v_set)):
                        v_i = v_set[j]
                        for u_in in pruned_item_map_user.get(v_i, []):
                            batch_vn[i][j-1][u_in] = pruned_item_map_user[v_i][u_in]
                        batch_true_u_v[i][j][0] = 0.0

                _, loss_val, pred_value = sess.run([train_step, gmf_loss, pred_val],feed_dict={one_hot_u: batch_u, one_hot_vp: batch_vp,one_hot_vn: batch_vn,one_true_u_v: batch_true_u_v})
                one_epoch_loss += loss_val
                one_epoch_batchnum += 1.0
                if index == int(len(pruned_all_ratings) / batch_size -1):
                    training_time = time() - t_epoch_begin
                    format_str = '%d epoch, averge loss = %.4f, training time: %.4fs'
                    print(format_str % (epoch, one_epoch_loss / one_epoch_batchnum, training_time))
                    ##################################################################--------evaluation for one epoch------##########################################################
                    hr_list = []
                    ndcg_list = []
                    t_evaa = time()
                    prediction_time = 0
                    for u_i in latest_item_interaction:
                        t_eva_begin = time()
                        v_random = set_4_batch_test[u_i][0]
                        for i in range(100):
                            for v_in in pruned_user_map_item[u_i]:
                                batch_u_test[i][v_in] = pruned_user_map_item[u_i][v_in]
                            for u_in in pruned_item_map_user.get(v_random[i], []):
                                batch_v_test[i][u_in] = pruned_item_map_user[v_random[i]][u_in]
                        pred_value = sess.run([pred_val], feed_dict={one_hot_u: batch_u_test, one_hot_vp: batch_v_test})
                        pre_real_val = np.array(pred_value).reshape((-1))
                        for i in range(100):
                            for v_in in pruned_user_map_item[u_i]:
                                batch_u_test[i][v_in] = 0.0
                            for u_in in pruned_item_map_user.get(v_random[i], []):
                                batch_v_test[i][u_in] = 0.0
                        items = v_random
                        gtItem = items[0]
                        map_item_score = {}
                        for i in range(len(items)):
                            item = items[i]
                            map_item_score[item] = pre_real_val[i]
                        ranklist = heapq.nlargest(top_k, map_item_score, key=map_item_score.get)
                        prediction_time = prediction_time + time() - t_eva_begin
                        hr_list.append(getHitRatio(ranklist, gtItem))
                        ndcg_list.append(getNDCG(ranklist, gtItem))

                    hr_val, ndcg_val = np.array(hr_list).mean(), np.array(ndcg_list).mean()
                    print('epoch %d: HR = %.4f, NDCG = %.4f, prediction time: %.4f s' % (epoch, hr_val, ndcg_val, prediction_time))
                    if hr_val > max(final_hr_metric_list):
                        best_time = t_evaa - t_begin
                        best_iter = epoch
                        if not trained:
                            saver.save(sess, model_path)
                            print("save done")

                    final_hr_metric_list.append(hr_val)
                    final_ndcg_metric_list.append(ndcg_val)

        best_hr = max(final_hr_metric_list)
        best_ndcg = max(final_ndcg_metric_list)
        print("End. Best epoch = %d:  HR = %.4f, NDCG = %.4f, best training time = %.4f s." % (best_iter, best_hr, best_ndcg, best_time))



